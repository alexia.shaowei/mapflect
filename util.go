package mapflect

import (
	"errors"
	"reflect"
	"strings"
)

func newMarshaler(mOut *marshalerOut) (*marshaler, error) {
	val := reflect.ValueOf(mOut.out)
	if val.Kind() != reflect.Ptr {
		return nil, errors.New("new out fail, should be a pointer")
	}

	val = val.Elem()
	if !val.CanAddr() {
		return nil, errors.New("new out fail, should be addressable")
	}

	mOut.mappingName = strings.EqualFold

	out := &marshaler{
		output: mOut,
	}

	return out, nil
}

func getNumKind(val reflect.Value) reflect.Kind {
	kind := val.Kind()

	switch {
	case kind >= reflect.Int && kind <= reflect.Int64:
		return reflect.Int

	case kind >= reflect.Uint && kind <= reflect.Uint64:
		return reflect.Uint

	case kind >= reflect.Float32 && kind <= reflect.Float64:
		return reflect.Float32

	default:
		return kind

	}
}

// func isNilValue(val reflect.Value) bool {

// 	switch getNumKind(val) {
// 	case reflect.Array, reflect.Map, reflect.Slice, reflect.String:
// 		return val.Len() == 0

// 	case reflect.Bool:
// 		return !val.Bool()

// 	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
// 		return val.Int() == 0

// 	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
// 		return val.Uint() == 0

// 	case reflect.Float32, reflect.Float64:
// 		return val.Float() == 0

// 	case reflect.Interface, reflect.Ptr:
// 		return val.IsNil()

// 	}

// 	return false
// }
